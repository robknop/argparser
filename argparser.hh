#ifndef INCLUDE_ARGPARSER_HH
#define INCLUDE_ARGPARSER_HH 1

// TODO
// : think about multiple value specification and defaults

#include <memory>
#include <string>
#include <list>
#include <map>
#include <vector>

namespace argparser {

class ArgParser;

/**
 * \brief The base class for the Argument template class.
 *
 * This exists so you can create a list of arguments of different types,
 * without having to do all kinds of complicated messing about with
 * std::any and casting and so forth.
 *
 */
class ArgumentBase {
public:
  /**
   * \brief Indicates whether this argument was passed in argc/argv.
   *
   * This is only valid after you've called the parse() method of the
   * ArgParser object to which you added this Argument.
   */
  bool given;

  ArgumentBase( const ArgumentBase& ) = delete;
  ArgumentBase( const ArgumentBase&& ) = delete;
  ArgumentBase& operator=(const ArgumentBase& ) = delete;
  ArgumentBase& operator=(const ArgumentBase&& ) = delete;

protected:
  ArgumentBase();

  bool ispositional, required;
  int minnum, maxnum;
  std::string help, name, shortflag, longflag;
  int flagdex, enddex;
  virtual bool ismatch( std::string &flag ) = 0;
  virtual int consume( std::vector<std::string> &argv, int dex0, int dex1 ) = 0;

  virtual std::string strdefault() const = 0;

  friend class ArgParser;
};

/**
 * \brief Define an argument for use with ArgParser
 *
 * Supported types include: int, unsigned int, float, double, string,
 * bool
 *
 * (bool is sort of a special case)
 *
 * Pass the Argument to the add_arg (or, in a list, to add_args) of an
 * ArgParser object.  When you've passed all the arguments you want,
 * call the parse method of ArgParser.
 *
 * After that, check given to see if the argument was provided on the
 * command line.  (This is how you handle simple switch flags.)  Call
 * get_val() for an argument that was not multiple-valued (i.e. where
 * you didn't specify a minnum and a maxnum) to get the specified value
 * argument, or get_vals() (if you did specify minnum and maxnum) to get
 * a vector of the values for that argument.  (If you call get_vals() on
 * an argument that only took one value, you'll get a length-1 vector.
 * If you call get_val() on an argument where multiple values were
 * supplied, you'll get the first one.)
 */
template<class T> class Argument : public ArgumentBase {
public:
  Argument( const std::string &shortflag, const std::string &longflag, const std::string &name,
            const T &defaultvalue,  const std::string &help="", int minnum=-1, int maxnum=-1,
            bool required=false );

  /**
   * \brief Return the value of this argument for single-valued arguments.
   *
   * Only look at this after you've called the parse() method of the
   * ArgParser object to which you added this argument.
   *
   * It will be what was parsed from the command line if the argument
   * was in argc/argv; otherwise, it will be the default value.
   *
   * If you don't want a default when nothing is explicitly specified,
   * check the .given property (see ArgumentBase) to find out if this
   * argument was present in argc/argv.
   *
   */
  const T& get_val() const;

  /**
   * \brief Return the value of this argument for multi-valued arguments.
   *
   * See get_val() re: given and defaults.
   *
   * (Multi-valued defaults aren't properly implemented at the moment!)
   *
   */
  const std::vector<T>& get_vals() const;

  Argument( const Argument<T>& ) = delete;
  Argument( const Argument<T>&& ) = delete;
  Argument<T>& operator=( const Argument<T>& ) = delete;
  Argument<T>& operator=( const Argument<T>&& ) = delete;

protected:
  bool ismatch( std::string &flag );
  int consume( std::vector<std::string> &argv, int dex0, int dex1 );
  std::string strdefault() const;

private:
  T defaultvalue;
  std::vector<T> vals;
  T val;
  T parse_one( std::string &str ) const;
};

/**
 * \brief A class for parsing command line arguments.
 *
 * It's a singleton; your executable can only have one.  Get a pointer
 * to it with ArgParser::get().  (Don't free the pointer.)
 *
 * This means, for instance, that it's a bad idea to build up an arg
 * parser in a constructor of another non-singleton object.  If you make
 * more than one of those objects, you will become confused (and
 * probably get errors as you try to add the same argument more than
 * once).
 *
 * Once you have the ArgParser instance, add arguments by making
 * Argument<T> objects and passing them to this class' add_arg() method.
 * You can also make a list of shared pointers to Argument<T> objects
 * and pass them to add_args.  As a convenience, you can make a map of
 * strings to shared pointers to Argument<T> objects and pass them to
 * add_args; in this case, it ignores the left side of the map, and just
 * adds the arguments on the right side of the map.  (This exists so you
 * can keep your own map of name->argument if you want.)
 *
 * When all arguments are added, call parse(argc, argv) (where argc and
 * argv are the arguments passed to your main()).  That will populate
 * the necessary fields of the Argument<T> objects, which you can then
 * read.  It will also strip out the parsed elements of argv (and adjust
 * argc accordingly).
 *
 * If you call add_arg (or add_args) again after parsing, the new
 * arguments will be added to the old arguments; subsequent parses will
 * consume the new ones.  (If you have manually futzed with argc and
 * argv, subsequent callings will also consume the old ones.  If you
 * haven't, then they will have been consumed by the first call.)  Be
 * caseful doing this, especially with positional parameters!  It is
 * easy to become confused.
 *
 */
class ArgParser {
public:
  /**
   * \brief return the singleton ArgParser instance
   *
   */
  static ArgParser* get();

  void add_arg( ArgumentBase &arg );
  void add_args( std::list<std::shared_ptr<ArgumentBase>> &args );
  void add_args( std::map<std::string,std::shared_ptr<ArgumentBase>> &args );

  /**
   * \brief Remove a previously existing Argument.
   *
   * @param name : Name of the argument to remove.
   *
   * Use cases for this function tend to be a bit obtuse.
   */
  void remove_arg( const std::string &name );

  /**
   * \brief parse arguments out of argc and argv.
   *
   * Parse arguments.  Removes anything consumed from argv.  Will update
   * the contents of the various Argument objects that have been added
   * to the parser, so that the Argument .given property and get_val()
   * methods will reflect what was consumed from argc and argv.
   *
   * If bool_if_leftovers is true, will throw an exception if there are
   * leftover arguments in argv after everything the parser knows about
   * has been consumed.  If bool_if_leftovers is false, will update argc
   * and argv with the consumed arguments removed.
   *
   */
  void parse( int &argc, char** &argv, bool error_if_leftovers=true );

  /**
   * \brief Return a help string.
   *
   * Returns a formatted string with all the arguments' help text.
   * Use this, e.g., to implement a "--help" option.
   *
   */
  const std::string& help();

private:
  ArgParser();
  static std::unique_ptr<ArgParser> instance;
  std::list<ArgumentBase*> arguments;
  std::string helpstr;
  std::unique_ptr<std::unique_ptr<char[]>[]> argvstorage;
  std::unique_ptr<char*[]> argv;
};

}; // End of namespace

#endif


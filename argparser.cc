// ****
// #include <iostream>
// #include <mpi.h>
// ****
#include <argparser.hh>
#include <stdexcept>
#include <sstream>
#include <iomanip>
#include <ios>
#include <cstring>

using namespace argparser;

// **********************************************************************

ArgumentBase::ArgumentBase() {
  flagdex = -1;
  enddex = -1;
  given = false;
}

template<class T> Argument<T>::Argument( const std::string &shortflag,
                                         const std::string &longflag,
                                         const std::string &name,
                                         const T &defaultvalue,
                                         const std::string &help,
                                         int minnum,
                                         int maxnum,
                                         bool required ) :
  defaultvalue( defaultvalue )
{
  this->shortflag = shortflag;
  this->longflag = longflag;
  this->name = name;
  this->minnum = minnum;
  this->maxnum = maxnum;
  this->help = help;
  this->required = required;

  if ( this->minnum < 0 ) this->minnum = 1;
  if ( this->maxnum < 0 ) this->maxnum = 1;
  if ( this->minnum == 0 ) {
    std::stringstream str("");
    str << "Argument " << name << " must have a minnum >= 1";
    throw std::invalid_argument( str.str() );
  }
  if ( ( shortflag.size() == 0 ) && ( longflag.size() == 0 ) ) ispositional = true;
  else ispositional = false;
  vals.resize( this->minnum );
  for ( int i = 0 ; i < this->minnum ; ++i ) vals[i] = defaultvalue;
  val = defaultvalue;
}

template<> Argument<bool>::Argument( const std::string &shortflag,
                                     const std::string &longflag,
                                     const std::string &name,
                                     const bool &defaultvalue,
                                     const std::string &help,
                                     int minnum,
                                     int maxnum,
                                     bool required ) :
  defaultvalue( defaultvalue )
{
  this->shortflag = shortflag;
  this->longflag = longflag;
  this->name = name;
  this->minnum = minnum;
  this->maxnum = maxnum;
  this->help = help;
  this->required = required;

  if ( ( shortflag.size() == 0 ) && ( longflag.size() == 0 ) ) {
    std::stringstream str("");
    str << "Error for " << name << " : bool arguments can't be positional.";
    throw std::invalid_argument( str.str() );
  }
  if ( this->minnum < 0 ) this->minnum = 0;
  if ( this->maxnum < 0 ) this->maxnum = 0;
  ispositional = false;
  if ( ( this->minnum != 0 ) || ( this->maxnum != 0 ) ) {
    std::stringstream str("");
    str << "Bad values for argument " << name
        << ": minnum and maxnum must be 0 for a bool argumnent, but you passed "
        << minnum << ", " << maxnum;
    throw std::invalid_argument( str.str() );
  }
  if ( defaultvalue ) {
    std::stringstream str("");
    str << "Bool argument " << name << " default value must be false.";
    throw std::invalid_argument( str.str() );
  }
  vals.resize(1);
  vals[0] = false;
  val = false;
}


// **********************************************************************

template<class T> const T& Argument<T>::get_val() const {
  return val;
}

template<class T> const std::vector<T>& Argument<T>::get_vals() const {
  return vals;
}

// **********************************************************************

template<class T> bool Argument<T>::ismatch( std::string &flag ) {
  std::string shortmatch( shortflag );
  if ( shortmatch[0] != '-' ) shortmatch = "-" + shortflag;
  std::string longmatch( longflag );
  if ( longmatch[0] != '-' ) {
    if ( longmatch[0] != '-' ) {
      longmatch = "--" + longflag;
    }
    else {
      longmatch = "-" + longflag;
    }
  }
  if ( ( flag == shortmatch ) || ( flag == longmatch ) ) return true;
  return false;
}

// **********************************************************************

template<class T> int Argument<T>::consume( std::vector<std::string> &argv, int dex0, int dex1 ) {
  int flagoff = ( ispositional ? 0 : 1 );

  if ( ( dex1 - dex0 - flagoff ) < minnum ) {
    std::stringstream str("");
    str << "Argument " << name << " requires at least " << minnum << " values, you gave " << dex1 - dex0 - flagoff;
    throw std::range_error( str.str() );
  }
  int num = dex1 - dex0 - flagoff;
  if ( ( maxnum > 0 ) && ( num > maxnum ) ) num = maxnum;
  if ( num < minnum ) {
    std::stringstream str("");
    str << "Only " << num << " values for " << name << ", minimum is " << minnum;
    throw std::invalid_argument( str.str() );
  }
  vals.resize( num );
  for ( int i = 0, dex = dex0 + flagoff ; i < num ; ++i, ++dex ) {
    vals[i] = parse_one( argv[dex] );
  }
  val = vals[0];
  argv.erase( argv.begin() + dex0, argv.begin() + dex0 + num + flagoff );
  given = true;
  return num + flagoff;
}

template<> int Argument<bool>::consume( std::vector<std::string> &argv, int dex0, int dex1 ) {
  vals.resize( 1 );
  vals[0] = true;
  argv.erase( argv.begin() + dex0, argv.begin() + dex0 + 1 );
  given = true;
  return 1;
}

// **********************************************************************

template<> int Argument<int>::parse_one( std::string &str ) const {
  try {
    return std::stoi( str );
  } catch (...) {
    std::stringstream estr("");
    estr << "Error parsing " << str << " as an integer for " << name;
    throw std::invalid_argument( estr.str() );
  }
}

template<> unsigned int Argument<unsigned int>::parse_one( std::string &str ) const {
  try {
    return std::stoul( str );
  } catch (...) {
    std::stringstream estr("");
    estr << "Error parsing " << str << " as an unsigned integer for " << name;
    throw std::invalid_argument( estr.str() );
  }
}

template<> float Argument<float>::parse_one( std::string &str ) const {
  try {
    return std::stof( str );
  } catch (...) {
    std::stringstream estr("");
    estr << "Error parsing " << str << " as a float for " << name;
    throw std::invalid_argument( estr.str() );
  }
}

template<> double Argument<double>::parse_one( std::string &str ) const {
  try {
    return std::stod( str );
  } catch (...) {
    std::stringstream estr("");
    estr << "Error parsing " << str << " as a double for " << name;
    throw std::invalid_argument( estr.str() );
  }
}

template<> std::string Argument<std::string>::parse_one( std::string &str ) const {
  return str;
}

// **********************************************************************

template<class T> std::string Argument<T>::strdefault() const {
  std::stringstream str("");
  str << defaultvalue;
  return str.str();
}

template<> std::string Argument<bool>::strdefault() const {
  if ( defaultvalue ) return "true";
  else return "false";
}

// **********************************************************************
// **********************************************************************
// **********************************************************************

std::unique_ptr<ArgParser> ArgParser::instance = nullptr;

ArgParser* ArgParser::get() {
  if ( instance == nullptr ) instance = std::unique_ptr<ArgParser>( new ArgParser );
  return instance.get();
}

// **********************************************************************

ArgParser::ArgParser() {
  // Is this necessary?
  arguments.clear();
  helpstr = "";
}

// **********************************************************************

void ArgParser::add_arg( ArgumentBase &arg ) {
  std::stringstream str("");

  for ( auto argi = arguments.begin() ; argi != arguments.end() ; ++argi ) {
    if ( arg.ispositional ) {
      if ( (*argi)->ispositional && ( (*argi)->maxnum <= 0 ) ) {
        str.str("");
        str << "Error at " << arg.name << " : you can only have one unbounded multi-valued positional "
            << " argument, and it must be at the end.";
        throw std::invalid_argument( str.str() );
      }
    } else {
      if ( ( ( arg.shortflag.size() > 0 ) && ( arg.shortflag == (*argi)->shortflag ) ) ||
           ( ( arg.shortflag.size() > 0 ) && ( arg.shortflag == (*argi)->longflag ) ) ) {
        str.str("");
        str << "Can't add argument \"" << arg.shortflag << "\", it already exists.";
        throw std::invalid_argument( str.str() );
      }
      if ( ( ( arg.longflag.size() > 0 ) && ( arg.longflag == (*argi)->shortflag ) ) ||
           ( ( arg.longflag.size() > 0 ) && ( arg.longflag == (*argi)->longflag ) ) ) {
        str.str("");
        str << "Can't add argument \"" << arg.longflag << "\", it already exists.";
        throw std::invalid_argument( str.str() );
      }
    }
  }
  arguments.push_back( &arg );
}

// **********************************************************************

void ArgParser::add_args( std::list<std::shared_ptr<ArgumentBase>> &args ) {
  for ( auto li = args.begin() ; li != args.end() ; ++li ) {
    add_arg( *(*li) );
  }
}

// **********************************************************************

void ArgParser::add_args( std::map<std::string,std::shared_ptr<ArgumentBase>> &args ) {
  for ( auto mi = args.begin() ; mi != args.end() ; ++mi ) {
    add_arg( *(mi->second) );
  }
}

// **********************************************************************

void ArgParser::remove_arg( const std::string &name ) {
  auto ai = arguments.begin();
  while ( ai != arguments.end() ) {
    if ( (*ai)->name == name ) {
      ai = arguments.erase( ai );
    } else {
      ++ai;
    }
  }
}

// **********************************************************************

void ArgParser::parse( int &argc, char** &argv, bool error_if_leftovers ) {
  bool done;

  std::vector<std::string> args;
  for ( int i = 1 ; i < argc ; ++i ) {
    args.push_back( std::string( argv[i] ) );
  }

  // Find the locations of the flags

  for ( int i = 0 ; i < args.size() ; ++i ) {
    for ( auto argi = arguments.begin() ; argi != arguments.end() ; ++argi ) {
      if ( (*argi)->flagdex >= 0 ) continue;
      if ( (*argi)->ispositional ) continue;
      if ( (*argi)->ismatch( args[i] ) ) {
        // std::cerr << "Found argument " << (*argi)->name << std::endl;
        (*argi)->flagdex = i;
        i += (*argi)->minnum;
      }
    }
  }

  // Find the end of where each argument should parse

  for ( auto argi = arguments.begin() ; argi != arguments.end() ; ++argi ) {
    if ( (*argi)->flagdex < 0 ) continue;
    int end = (*argi)->flagdex + (*argi)->maxnum + 1;
    if ( end > args.size() ) end = args.size();
    for ( auto argj = arguments.begin() ; argj != arguments.end() ; ++argj ) {
      if ( ( (*argj)->flagdex > (*argi)->flagdex ) && ( (*argj)->flagdex < end ) ) end = (*argj)->flagdex;
    }
    (*argi)->enddex = end;
    // std::cerr << "Argument " << (*argi)->name << " is at "
    //           << (*argi)->flagdex << " : " << (*argi)->enddex << std::endl;
  }

  // Consume arguments
  for ( auto argi = arguments.begin() ; argi != arguments.end() ; ++argi ) {
    if ( (*argi)->flagdex < 0 ) continue;

    int neaten = (*argi)->consume( args, (*argi)->flagdex, (*argi)->enddex );
    // std::cerr << "Argument " << (*argi)->name << " consumed " << neaten << " from argv" << std::endl;
    for ( auto argj = arguments.begin() ; argj != arguments.end() ; ++argj ) {
      if ( (*argj)->flagdex > (*argi)->flagdex ) {
        (*argj)->flagdex -= neaten;
        (*argj)->enddex -= neaten;
      }
    }
  }

  // ****
  // std::cerr << "*****************************\n";
  // std::cerr << "Remaining parameters:";
  // for ( auto s=args.begin() ; s!=args.end() ; ++s ) std::cerr << " " << (*s);
  // std::cerr << std::endl;
  // for ( auto argi=arguments.begin() ; argi != arguments.end() ; ++argi ) {
  //   std::cerr << "Argument " << (*argi)->name << " : ispositional="
  //             << (*argi)->ispositional << ", flagdex="
  //             << (*argi)->flagdex << ", enddex="
  //             << (*argi)->enddex << std::endl;
  // }
  // std::cerr << "*****************************\n";
  // ****

  // Consume positional arguments

  for ( auto argi=arguments.begin() ; ( args.size() > 0 ) && ( argi != arguments.end() ) ; ++argi ) {
    if ( ! (*argi)->ispositional ) continue;
    int end = (*argi)->maxnum;
    if ( end == 0 ) end = args.size();
    else if ( end > args.size() ) end = args.size();
    for ( auto argj = arguments.begin() ; argj != arguments.end() ; ++argj ) {
      if ( ( ! (*argj)->ispositional ) && ( (*argj)->flagdex > 0 ) &&
           ( (*argj)->flagdex < end ) )
        end = (*argj)->flagdex;
    }
    // std::cerr << "Parsing positional " << (*argi)->name << " from 0 to " << end << std::endl;
    int neaten = (*argi)->consume( args, 0, end );
    for ( auto argj = arguments.begin() ; argj != arguments.end() ; ++argj ) {
      if ( ! (*argj)->ispositional ) {
        (*argj)->flagdex -= neaten;
        (*argj)->enddex -= neaten;
      }
    }
  }

  for ( auto argi=arguments.begin() ; argi!=arguments.end() ; ++argi ) {
    std::stringstream str("");
    str << "Missing required arguments: ";
    bool oops = false;
    if ( ( (*argi)->required ) && ( ! (*argi)->given ) ) {
      str << (*argi)->name << " ";
      oops = true;
    }
    if ( oops ) {
      throw std::invalid_argument( str.str() );
    }
  }

  if ( error_if_leftovers && ( args.size() > 0 ) ) {
    std::stringstream str("");
    str << "Unparsed arguments :";
    for ( auto s=args.begin() ; s != args.end() ; ++s ) str << " " << *s;
    throw std::invalid_argument( str.str() );
  }

  argc = args.size() + 1;

  auto newargv = std::make_unique<std::unique_ptr<char[]>[]>( argc );
  for ( int i = 0 ; i < argc ; ++i ) newargv[i] = nullptr;
  newargv[0] = std::make_unique<char[]>( strlen(argv[0])+1  );
  memcpy( newargv[0].get(), argv[0], strlen(argv[0])+1 );
  for ( int i = 0 ; i < args.size() ; ++i ) {
    newargv[i+1] = std::make_unique<char[]>( args[i].size()+1 );
    memcpy( newargv[i+1].get(), args[i].c_str(), args[i].size()+1 );
  }

  // I do this rather than starting with argvstorage or this->argv above
  // so that the old this->argv doesn't go out of scope too soon (as the
  // old this->argv() may well have been passed as the argv parameter!)
  argvstorage = std::make_unique<std::unique_ptr<char[]>[]>( argc );
  for ( int i = 0 ; i < argc ; ++i ) argvstorage[i] = std::move( newargv[i] );
  newargv.reset();
  // ...and, because we need to return a **char, we play even
  // more games.
  this->argv = std::make_unique<char*[]>( argc );
  for ( int i = 0 ; i < argc ; ++i ) this->argv[i] = argvstorage[i].get();
  argv = this->argv.get();
}

// **********************************************************************

const std::string& ArgParser::help() {
  if ( helpstr.size() > 0 ) return helpstr;

  std::stringstream ofp("");
  ofp.setf( std::ios::left );

  bool positionals = false;
  bool flagged = false;
  for ( auto argi=arguments.begin() ; argi != arguments.end() ; ++argi ) {
    if ( (*argi)->ispositional ) positionals = true;
    else flagged = true;
  }

  auto splitwords = []( const std::string& text ) {
    std::vector<std::string> words;
    int start, end;
    start = 0;
    end = 0;
    while ( end < text.size() ) {
      start = text.find_first_not_of( " \t\n", end );
      if ( start >= text.size() ) break;
      end = text.find_first_of( " \t\n", start );
      words.push_back( text.substr( start, end-start ) );
    }
    return words;
  };

  auto print_arg = [splitwords]( const std::list<ArgumentBase*>::const_iterator &argi, std::ostream &ofp ) {
    std::stringstream str("");
    str << "  " << (*argi)->name;
    if ( ( (*argi)->shortflag.size() > 0 ) || ( (*argi)->longflag.size() > 0 ) ) {
      str << " (";
      if ( (*argi)->shortflag.size() > 0 ) {
        str << (*argi)->shortflag;
        if ( (*argi)->longflag.size() > 0 ) str << ",";
      }
      if ( (*argi)->longflag.size() > 0 ) str << (*argi)->longflag;
      str << ")";
    }
    ofp << std::setw(32) << str.str() << " --";
    auto words = splitwords( (*argi)->help );
    int pos = 36;
    for ( auto wordi=words.begin() ; wordi!=words.end() ; ++wordi ) {
      if ( ( pos + 1 + wordi->size() ) > 80 ) {
        ofp << std::endl << "                                   ";
        pos = 36;
      }
      ofp << " " << (*wordi);
    }
    if ( ! (*argi)->required ) {
      str.str( "" );
      str << " [Default: " << (*argi)->strdefault() << "]";
      if ( pos + str.str().size() > 80 ) {
        ofp << std::endl << "                                   ";
        pos = 36;
      }
      ofp << str.str();
    }
    else {
      str.str("");
      str << " [required]";
      if ( pos + str.str().size() > 80 ) {
        ofp << std::endl << "                                   ";
        pos = 36;
      }
      ofp << str.str();
    }
    ofp << std::endl;
  };

  if ( positionals ) {
    ofp << "Positional Arguments" << std::endl;
    for ( auto argi=arguments.begin() ; argi != arguments.end() ; ++argi )
      if ( (*argi)->ispositional )
        print_arg( argi, ofp );
  }

  if ( flagged ) {
    ofp << std::endl << "Flagged arguments" << std::endl;
    for ( auto argi=arguments.begin() ; argi != arguments.end() ; ++argi )
      if ( ! (*argi)->ispositional )
        print_arg(argi, ofp );
  }

  ofp << std::endl;
  helpstr = ofp.str();
  return helpstr;
}

// **********************************************************************
// Force implementations

template class Argument<int>;
template class Argument<unsigned int>;
template class Argument<float>;
template class Argument<double>;
template class Argument<bool>;
template class Argument<std::string>;

// template void ArgParser::add_arg<int>( Argument<int>& );
// template void ArgParser::add_arg<int>( Argument<unsigned int>& );
// template void ArgParser::add_arg<double>( Argument<float>& );
// template void ArgParser::add_arg<double>( Argument<double>& );
// template void ArgParser::add_arg<bool>( Argument<bool>& );
// template void ArgParser::add_arg<std::string>( Argument<std::string>& );
